﻿using System;
using System.IO;
using System.Linq;

namespace MyExtensions
{
    public static class StringExtension
    {
        public static int WordCount(this String str)
        {
            return str.Split(new char[] { ' ', '.', '?' },
                StringSplitOptions.RemoveEmptyEntries).Length;
        }
    }

    public static class Formatter
    {
        public static string NameFormat(this DirectoryInfo dir)
        {
           return $"{dir.Name} ({dir.GetFileSystemInfos().Length})";
        }

        public static string NameFormat(this FileInfo file)
        {
            return $"{file.Name} {file.Length} bytes";
        }

        public static long GetDirSize(this DirectoryInfo dir)
        {
            //Using LINQ
            return dir.EnumerateFiles("*.*", SearchOption.AllDirectories).Sum(fi => fi.Length);
        }

        public static long CountFiles(this DirectoryInfo dir)
        {
            return dir.EnumerateFiles("*.*", SearchOption.AllDirectories).Count(fi => fi.Exists);
        }
        public static FileSystemInfo GetOldest(this DirectoryInfo dInfo)
        {
            return dInfo.GetFileSystemInfos().OrderBy(fi => fi.CreationTime).First();
        }
        public static long GetCharOccurences(this FileInfo file, char toFind)
        {
            //Faster than LINQ method
            long count = 0;
            foreach (var c in file.Name)
            {
                if (c == toFind)
                {
                    count++;
                }
            }

            return count;
        }
    }

}
