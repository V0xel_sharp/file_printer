using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using MyExtensions;

namespace Lab1
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            if (args.Length != 1)
            {
                Console.Error.WriteLine("Invalid number of arguments.");
                return;
            }
            if (Directory.Exists(args[0]) == false)
            {
                Console.Error.WriteLine("Invalid path.");
                return;
            }

            //Wrapping and processing directory
            var dInfo = new DirectoryInfo(args[0]);
            ProcessDirectory(dInfo);
            
            //Additional stuff
            Console.WriteLine($"Size of current directory: {dInfo.GetDirSize()} bytes");
            Console.WriteLine($"Number of files in directory: {dInfo.CountFiles()}");
            Console.WriteLine($"The oldest file in dir: {dInfo.GetOldest().Name}, created at {dInfo.GetOldest().CreationTime}");

            //Creating and initilizaing the collection
            var sortedCollection = new SortedDictionary<string, long>();
            foreach (var e in dInfo.EnumerateFileSystemInfos())
            {
                switch (e)
                {
                    case DirectoryInfo d:
                        sortedCollection.Add(d.Name, d.GetFileSystemInfos().Length);
                        break;
                    case FileInfo f:
                        sortedCollection.Add(f.Name, f.Length);
                        break;
                }
            }

            //Serialization
            var fs = new FileStream("DataFile.dat", FileMode.Create);
            var formatter = new BinaryFormatter();
            try
            {
                formatter.Serialize(fs, sortedCollection);
            }
            catch (SerializationException e)
            {
                Console.WriteLine("Failed to serialize cause:" + e.Message);
                throw;
            }
            finally
            {
                fs.Close();
            }

            //Deserialization
            fs = new FileStream("Datafile.dat", FileMode.Open);
            try
            {
                sortedCollection = (SortedDictionary<string, long>) formatter.Deserialize(fs);
            }
            catch (SerializationException e)
            {
                Console.WriteLine("Failed to deserialize cause: " + e.Message);
                throw;
            }
            finally
            {
                fs.Close();
            }

            //Print for check
            foreach (var kvp in sortedCollection)
            {
                Console.WriteLine($"{kvp.Key}, {kvp.Value}");
            }
        }

        //Function for dir processing
        private static void ProcessDirectory(DirectoryInfo dir, int depth = 0)
        {
            Console.WriteLine(dir.NameFormat().PadLeft(dir.NameFormat().Length + depth, '\t'));
            var fileCollection = dir.EnumerateFiles();
            ++depth;

            foreach (var file in fileCollection)
            {
                Console.WriteLine(file.NameFormat().PadLeft(file.NameFormat().Length + depth, '\t'));
            }

            var dirCollection = dir.EnumerateDirectories();
            foreach (var d in dirCollection)
            {
                ProcessDirectory(d, depth);
            }
        }
    }
}
